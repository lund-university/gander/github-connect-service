# PuR Service
The PuR service handles the requests for the files, pull requests, and commits in Pygmy. 
It is mostly in charge of communicating with GitHub, so the service needs a GitHub Authentication token (see more about that below).

## Folder Structure

The workspace contains two folders by default, where:

- `app/src`: the folder to maintain sources
- `lib`: the folder to maintain dependencies


## GitHub Authentication
Annotation Service needs a file called `credentials.txt` to be able to fetch comments and work properly with GitHub. If you don't have a personal access token in the header of your HTTP-request, after approx. 50 requests you will get an error code 403: Forbidden. 

This require you to:
* Have a GitHub account
* [Follow this tutorial for creating a GitHub personal access token](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token). I recommend having a separate one for working with Pygmy.  
* Save the information in a file called `credentials.txt`. It should be located inside the `/app` folder. 
* The file should contain `username:AuthToken`, where username is your username on GitHub and AuthToken the token you just created. The username and the AuthToken should be separated by a `:`.
* This token is used in more services, so copy the file into them as well. Read the separate `README.md` files to see if the services need the authentication token. It is okay to use the same access token in multiple service. username:AuthToken
     

## Dependency Management

The `JAVA DEPENDENCIES` view allows you to manage your dependencies. More details can be found [here](https://github.com/microsoft/vscode-java-pack/blob/master/release-notes/v0.9.0.md#work-with-jar-files-directly).

## Run
To run the PuR service, run:

On windows: ```gradle run```
On mac:  ```./gradlew run```

How to test the server in the terminal:

To add a file to review:  `curl -d '{"name":"fileName", "content":"content of file"}' -X POST http://localhost:8002/pur`

To update a file: `curl -d '{"content":"new content of file"}' -X PUT http://localhost:8002/pur/{pathToFile}`

To get all added projects: `curl -X GET localhost:8002/purs`

To get test the ping-pong service: `curl -X GET localhost:8002/ping`

To get the content of a file: `curl -X GET localhost:8002/pur/{pathToFile}`

To get the content of a file from a specific commit: `curl -X GET localhost:8002/pur/{pathToFile}?sha={commitSha}`

To get the all commits: `curl -X GET localhost:8002/pur/commits`

To get the content of a commit: `curl -X GET localhost:8002/pur/commits/{commitSha}`

To get all commits to a specific file: `curl -X GET 'localhost:8002/pur/commits?name={pathToFile}'`

To get the content of the git repository: `curl -X GET localhost:8002/git`

To get your rate limit status from the GitHub API: `curl -X GET localhost:8002/git/limit`

To fromat the code with according to google-java-format:  `./gradlew goJF`

To compare two files: `curl -X GET localhost:8002/pur/compare/{sha1}...{sha2}`

To get the latest version of a file until a date: `curl -X GET localhost:8002/pur/{pathToFile}/{date}`

To format the code and start the server with the same gradle command: `./gradlew cleanrun`

To get all pull requests (open and closed): `curl -X GET localhost:8002/git/pulls`
To get a specific pull request: `curl -X GET localhost:8002/git/pulls/{pull_number}`


## Test
To run the unit test:

On windows: 
```gradle test``` 
use ```gradle cleanTest test``` to always see the test results.  

On mac:
```./gradlew test``` 
use ```./gradlew cleanTest test``` to always see the test results.


