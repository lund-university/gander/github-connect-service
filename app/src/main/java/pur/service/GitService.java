package pur.service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import spark.Request;
import spark.Response;

/** Class for handling the storing and retrieval of files and folders from Git. */
class GitService {
  private static final String GIT_OWNER = System.getenv("GIT_OWNER") != null ? System.getenv("GIT_OWNER") : "granttitus";
  private static final String GIT_REPO = System.getenv("GIT_REPO") != null ? System.getenv("GIT_REPO") : "FlappyBird";
  private static final String GIT_ADDRESS = System.getenv("GIT_ADDRESS") != null ? System.getenv("GIT_ADDRESS") : "https://api.github.com";
  private static final String GIT_REPO_PATH = "repos";
  private static final String PUR_FILE = "file";
  // private static final String PUR_FOLDER = "dir";
  private static final String RATE_LIMIT = "rate_limit";
  Logger logger;
  private static final String CLASS_NAME = PingService.class.getName();

  private PurService pur;

  /**
   * @param logger The Logger object used to debug log.
   * @param pur The Pur Service.
   */
  public GitService(PurService pur, Logger log) {
    this.pur = pur;
    this.logger = log;
    logger.info("GitService started");
  }

  /**
   * Receives the list of commits from GitHub.
   *
   * @param req The Request object.
   * @param res The Response object.
   * @return A JSON String containing the Commit list.
   */
  public String getCommits(Request req, Response res) {
    String name = req.queryParams("name");
    String url =
        String.format("%s/%s/%s/%s/commits", GIT_ADDRESS, GIT_REPO_PATH, GIT_OWNER, GIT_REPO);

    if (name != null) {
      url = url + "?path=" + name;
    }
    logger.entering(CLASS_NAME, "getCommits", url);

    try {
      HttpURLConnection c = HttpConnectionUtils.setGithubConnection(url, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonString = JsonUtils.processGithubResponse(c.getInputStream());
        res.status(HttpURLConnection.HTTP_OK);

        logger.exiting(CLASS_NAME, "getCommits", jsonString);
        return processCommitList(jsonString);
      } else {
        res.status(c.getResponseCode());
        String err =
            String.format(
                "%d: Error while geting from git: %s", c.getResponseCode(), c.getResponseMessage());
        logger.severe(err);
        return err;
      }
    } catch (IOException e) {
      logger.severe(e.getMessage());
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      return String.format("Invalid path to git repo: %d", e.getMessage());
    }
  }

  /**
   * Gets the information of a commit with the specified ":ref".
   *
   * @param req The Request object. Should contain the param ":ref".
   * @param res The Response object.
   * @return The commit information as a JSON String.
   */
  public String getCommitContent(Request req, Response res) {

    String ref = req.params(":ref");
    String url =
        String.format(
            "%s/%s/%s/%s/commits/%s", GIT_ADDRESS, GIT_REPO_PATH, GIT_OWNER, GIT_REPO, ref);
    logger.entering(CLASS_NAME, "getCommitContent", url);
    try {
      HttpURLConnection c = HttpConnectionUtils.setGithubConnection(url, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonString = JsonUtils.processGithubResponse(c.getInputStream());
        res.status(HttpURLConnection.HTTP_OK);
        logger.exiting(CLASS_NAME, "getCommitContent", jsonString);
        return processCommitInfo(jsonString);
      } else {
        res.status(c.getResponseCode());
        String errGit =
            String.format(
                "%d: Error while geting from git %s", c.getResponseCode(), c.getResponseMessage());
        logger.severe(errGit);
        return errGit;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errPath = String.format("Invalid path to git repo: %d", e.getMessage());
      logger.severe(errPath);
      return errPath;
    }
  }

  /**
   * Takes the response from GitHub and extracts the commit information.
   *
   * @param s The response String from GitHub.
   * @return A JSON String with the commit information.
   */
  private String processCommitInfo(String s) {
    logger.entering(CLASS_NAME, "processCommitInfo", s);
    JSONObject commitInformation = JsonUtils.createCommitInfo(s);
    logger.exiting(CLASS_NAME, "processCommitInfo", commitInformation);
    return commitInformation.toString();
  }

  /**
   * Receives the rate limit we have towards GitHub. Can be used to check if we are authenticated or
   * not. If not, we will have a much lower rate limit.
   *
   * @param req The Request object.
   * @param res The Response object.
   * @return A JSON String containing rate limit.
   */
  public String getRateLimit(Request req, Response res) {
    String url = String.format("%s/%S", GIT_ADDRESS, RATE_LIMIT);
    logger.entering(CLASS_NAME, "getRateLimit", url);
    try {
      HttpURLConnection c = HttpConnectionUtils.setGithubConnection(url, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonString = JsonUtils.processGithubResponse(c.getInputStream());
        res.status(HttpURLConnection.HTTP_OK);
        logger.exiting(CLASS_NAME, "getRateLimit", jsonString);
        return jsonString;
      } else {
        res.status(c.getResponseCode());
        String errGit =
            String.format(
                "%d: Error while geting rate limit from git %s",
                c.getResponseCode(), c.getResponseMessage());
        logger.severe(errGit);
        return errGit;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errPath = String.format("Invalid path to git repo: %d", e.getMessage());
      logger.severe(errPath);
      return errPath;
    }
  }

  /**
   * Receives the file from GitHub. This is used in the situations that the file is not yet in
   * PurService, for example when a file has been deleted or added.
   *
   * @param name The name of the file or folder we are looking for.
   * @param res The Response object.
   * @return The PurContent object.
   */
  private PurContent getGitFileFromName(String name, Response res) {
    String url =
        String.format(
            "%s/%s/%s/%s/contents/%s", GIT_ADDRESS, GIT_REPO_PATH, GIT_OWNER, GIT_REPO, name);
    logger.entering(CLASS_NAME, "getGitFileFromName", url);
    try {
      HttpURLConnection c = HttpConnectionUtils.setGithubConnection(url, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonString = JsonUtils.processGithubResponse(c.getInputStream());
        res.status(HttpURLConnection.HTTP_OK);
        JSONObject obj = JsonUtils.parseJSONObject(jsonString);
        if (obj.get("type").equals(PUR_FILE)) {
          logger.exiting(CLASS_NAME, "getGitFileFromName", obj);
          return createSingleFile(name, obj.get("sha").toString(), obj.get("content").toString());
        } else {
          logger.exiting(CLASS_NAME, "getGitFileFromName", obj);
          return createFolder(name, obj.get("sha").toString());
        }
      } else {
        res.status(c.getResponseCode());
        logger.severe(String.format("%d: Could not find git file/folder.", c.getResponseCode()));
        return null;
      }
    } catch (IOException e) {
      String errPath = String.format("Invalid path to git repo: %d", e.getMessage());
      logger.severe(errPath);
      return null;
    }
  }

  /**
   * Receives the content or the PurContent with the name ":name".
   *
   * @param req The Request object. Should contain the param ":name". Can have the param
   *     ":nestedFile" if we are looking for a file in a folder.
   * @param res The Response object.
   * @return A JSON String containing content of the PurContent.
   */
  public String getContent(Request req, Response res) {
    String path = req.params(":name");
    if (req.params(":nestedFile") != null) {
      path += "/" + req.params(":nestedFile");
    }
    String sha = req.queryParams("sha");
    if (sha != null) {
      path = path + "?ref=" + sha;
    }
    logger.entering(CLASS_NAME, "getContent", path);
    JSONObject obj = new JSONObject();
    PurContent file = pur.getPurContentByName(path);
    obj.put("name", path);
    if (file == null) {
      file = getGitFileFromName(path, res);
      if (file == null) {
        obj.put("error", "file name not found");
        res.body(obj.toString());
        res.status(HttpURLConnection.HTTP_NOT_FOUND);
        logger.exiting(CLASS_NAME, "getContent", obj.toString());
        return obj.toString();
      }
      pur.addPur(file);
    }

    res.status(HttpURLConnection.HTTP_OK);

    if (file.isFile()) {
      String content;

      if (((PurFile) file).getContent().equals("")) {
        content = getFileContent(path, res);
      } else {
        content = ((PurFile) file).getContent();
      }

      obj.put("content", content);
    } else {
      String files = getFolderContent(path, res);
      JSONArray arr = JsonUtils.makeFolderFileArray(((PurFolder) file).getFolderFiles());
      obj.put("files", arr);
    }
    obj.put("commits", pur.createShas(file.getCommits()));
    obj.put("name", file.getName());

    logger.entering(CLASS_NAME, "getContent", obj.toString());
    return obj.toString();
  }

  /**
   * Receives all of the content in the Git repo.
   *
   * @param req The Request object.
   * @param res The Response object.
   * @return A JSON String containing list of files and folders in the repo.
   */
  public String getFilesInGitRepo(Request req, Response res) {
    String url =
        String.format("%s/%s/%s/%s/contents", GIT_ADDRESS, GIT_REPO_PATH, GIT_OWNER, GIT_REPO);
    logger.entering(CLASS_NAME, "getFilesInGitRepo", url);
    try {
      HttpURLConnection c = HttpConnectionUtils.setGithubConnection(url, "GET");
      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonArrString = JsonUtils.processGithubResponse(c.getInputStream());
        res.status(HttpURLConnection.HTTP_OK);
        String result = createRepoContent(jsonArrString, res).toString();

        res.body(result);
        logger.exiting(CLASS_NAME, "getFilesInGitRepo", result);
        return result;
      } else {
        res.status(c.getResponseCode());
        String errGit =
            String.format("%d: Error while getting files in a repo from git", c.getResponseCode());
        logger.severe(errGit);
        return errGit;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path to git repo: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  /**
   * Takes the response String and returns a JSON String with all of the commits.
   *
   * @param s The response String from GitHub.
   * @return A JSON String with the list of commits.
   */
  private String processCommitList(String s) {
    logger.entering(CLASS_NAME, "processCommitList", s);
    JSONArray commitList = JsonUtils.createCommitList(s);
    logger.exiting(CLASS_NAME, "processCommitList", commitList);
    return commitList.toString();
  }

  /**
   * Takes the response from GitHub containing all of the content and adds it to the local storage.
   *
   * @param inputArray the response String from GitHub.
   * @param res The HTTP Response object.
   * @return A JSON Array with all of the found content.
   */
  private JSONArray createRepoContent(String inputArray, Response res) {
    logger.entering(CLASS_NAME, "createRepoContent", inputArray);
    JSONParser jsonParser = new JSONParser();
    JSONArray result = new JSONArray();
    JSONArray jsonArr = JsonUtils.parseJsonArray(inputArray);
    Iterator<JSONObject> iterator = jsonArr.iterator();
    while (iterator.hasNext()) {
      JSONObject obj = iterator.next();
      String path = obj.get("path").toString();

      if (obj.get("type").equals("dir")) {
        pur.addPur(createFolder(obj.get("path").toString(), obj.get("sha").toString()));
        result.add(
            JsonUtils.parseJSONObject(
                "{ \"name\": \""
                    + obj.get("path")
                    + "\", \"type\" : \""
                    + obj.get("type")
                    + "\" }"));
      } else {
        String content = getFileContent(path, res);
        PurFile newFile =
            createSingleFile(obj.get("path").toString(), obj.get("sha").toString(), content);
        pur.addPur(newFile);

        result.add(newFile.toJSON());
      }
      // Not covering the other cases like submodules. Perhaps a TODO?
    }
    logger.exiting(CLASS_NAME, "createRepoContent", result);
    return result;
  }

  /**
   * Takes the response from GitHub containing all of the files and adds it to the local storage.
   *
   * @param inputArray the response String from GitHub.
   * @param res The HTTP Response object.
   * @return A JSON Array with all of the found files.
   */
  private JSONArray createFiles(String inputArray, Response res) {
    logger.entering(CLASS_NAME, "createFiles", inputArray);
    JSONParser jsonParser = new JSONParser();
    JSONArray result = new JSONArray();
    JSONArray jsonArr = JsonUtils.parseJsonArray(inputArray);
    Iterator<JSONObject> iterator = jsonArr.iterator();
    while (iterator.hasNext()) {
      JSONObject obj = iterator.next();
      String path = obj.get("path").toString();
      String content = getFileContent(path, res);
      pur.addPur(new PurFile(obj.get("path").toString(), obj.get("sha").toString(), content));
      result.add(
          JsonUtils.parseJSONObject(
              "{ \"name\": \"" + obj.get("path") + "\", \"sha\" : \"" + obj.get("sha") + "\" }"));
    }
    logger.exiting(CLASS_NAME, "createFiles", result);
    return result;
  }

  /**
   * Creates a PurFile with the specified attributes.
   *
   * @param path The name of the file.
   * @param sha The sha of the file.
   * @param content The content of the file. 
   * @return A PurFile with the specified attributes.
   */
  private PurFile createSingleFile(String path, String sha, String content) {
    logger.entering(CLASS_NAME, "createSingleFile", path);
    logger.exiting(CLASS_NAME, "createFolder", path);
    return new PurFile(path, sha, content);
  }

  /**
   * Creates a PurFolder with the specified attributes.
   *
   * @param path The name of the folder.
   * @param sha The sha of the folder.
   * @return A PurFolder with the specified attributes.
   */
  private PurFolder createFolder(String path, String sha) {
    logger.entering(CLASS_NAME, "createFolder", path);
    logger.exiting(CLASS_NAME, "createFolder", path);
    return new PurFolder(path, sha);
  }

  /**
   * Gets the content of a file with the name "path".
   *
   * @param path The name of the filer we want to receive the content of.
   * @param res The Response object.
   * @return A JSON String containing the file content.
   */
  private String getFileContent(String path, Response res) {
    String url =
        String.format(
            "%s/%s/%s/%s/contents/%s", GIT_ADDRESS, GIT_REPO_PATH, GIT_OWNER, GIT_REPO, path);

    logger.entering(CLASS_NAME, "getFileContent", url);
    try {
      HttpURLConnection c = HttpConnectionUtils.setGithubConnection(url, "GET");

      c.setRequestProperty("Accept", "application/vnd.github.v3+json");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonString = JsonUtils.processGithubResponse(c.getInputStream());
        res.status(HttpURLConnection.HTTP_OK);
        JSONObject json = JsonUtils.parseJSONObject(jsonString);
        logger.exiting(CLASS_NAME, "getFileContent", json.get("content").toString());
        return json.get("content").toString();
      } else {
        res.status(c.getResponseCode());
        return String.format(
            "%d: Error while geting from git:%s", c.getResponseCode(), c.getResponseMessage());
      }
    } catch (IOException e) {
      return String.format("Invalid path to git repo: %d", e.getMessage());
    }
  }

  /**
   * Gets the content of a folder with the name "path".
   *
   * @param path The name of the folder we want to receive the content of.
   * @param res The Response object.
   * @return A JSON String containing the folder content.
   */
  public String getFolderContent(String path, Response res) {
    String url =
        String.format(
            "%s/%s/%s/%s/contents/%s", GIT_ADDRESS, GIT_REPO_PATH, GIT_OWNER, GIT_REPO, path);
    logger.entering(CLASS_NAME, "getFolderContent", url);
    ArrayList<PurContent> foundFiles = new ArrayList<PurContent>();
    PurFolder folder = (PurFolder) pur.getPurContentByName(path);
    logger.fine("Folder " + folder.getName() + " was found");
    try {
      HttpURLConnection c = HttpConnectionUtils.setGithubConnection(url, "GET");

      c.setRequestProperty("Accept", "application/vnd.github.v3+json");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.fine("Request for " + path + " was OK");
        res.status(c.getResponseCode());
        String jsonString = JsonUtils.processGithubResponse(c.getInputStream());
        res.status(HttpURLConnection.HTTP_OK);
        JSONArray json = JsonUtils.parseJsonArray(jsonString);
        logger.fine("json array from response: " + json.toString());
        for (Object o : json) {
          JSONObject file = (JSONObject) o;
          logger.fine("Object in array " + file.toString() + " was parsed");

          if (file.get("type").equals("file")) {
            logger.fine("Found file " + file.get("name") + " with path " + file.get("path"));
            String content = getFileContent(file.get("path").toString(), res);
            if (!folder.containsFile(file.get("path").toString())) {
              PurFile newFile =
                  new PurFile(file.get("path").toString(), file.get("sha").toString(), content);
              folder.addFolderFile(newFile);
              pur.addPur(newFile);
              foundFiles.add(newFile);
            } else {
              PurContent newFile = pur.getPurContentByName(file.get("path").toString());
              foundFiles.add(newFile);
            }

            logger.finest("File was added to folder " + folder.getName());
          } else if (file.get("type").equals("dir")) {
            logger.finest(
                "File "
                    + file.get("path").toString()
                    + " found was a folder. Attempting to create its content");
            PurFolder insideFolder =
                new PurFolder(file.get("path").toString(), file.get("sha").toString());
            folder.addFolderFile(insideFolder);
            pur.addPur(insideFolder);
          }
        }
        logger.exiting(CLASS_NAME, "getFolderFiles", foundFiles.toString());
        return foundFiles.toString();
      } else {
        res.status(c.getResponseCode());
        String errGit =
            String.format(
                "%d: Error while geting from git:%s", c.getResponseCode(), c.getResponseMessage());
        logger.severe(errGit);
        return errGit;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path to git repo: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  /**
   * Receives a version of a file with a date earlier to ":date". This is used to be able to compare
   * versions of a file when performing pull requests.
   *
   * @param req The Request object. Should have the params ":name" and ":date".
   * @param res The Response object.
   * @return A JSON String containing the file content of that version.
   */
  public String getLatestVersionOfFileUntilDate(Request req, Response res) {
    String fileName = req.params(":name");
    String date = req.params(":date");
    String url =
        String.format(
            "%s/%s/%s/%s/commits?path=%s",
            GIT_ADDRESS, GIT_REPO_PATH, GIT_OWNER, GIT_REPO, fileName);
    logger.entering(CLASS_NAME, "getLatestVersionOfFileUntilDate", url);
    try {
      HttpURLConnection c = HttpConnectionUtils.setGithubConnection(url, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonString = JsonUtils.processGithubResponse(c.getInputStream());
        res.status(HttpURLConnection.HTTP_OK);
        String sha = findLatestCommitUntilDate(jsonString, date);
        String content;

        if (sha == null) {
          content = "";

        } else {
          String path = fileName + "?ref=" + sha;
          content = getFileContent(path, res);
        }

        JSONObject obj = new JSONObject();
        obj.put("content", content);

        logger.exiting(CLASS_NAME, "getLatestVersionOfFileUntilDate", obj.toString());
        return obj.toString();
      } else {

        res.status(c.getResponseCode());
        String errGit =
            String.format(
                "%d: Error while geting from git:%s", c.getResponseCode(), c.getResponseMessage());
        logger.severe(errGit);
        return errGit;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errExc = String.format("Invalid path to git repo: %d", e.getMessage());
      logger.severe(errExc);
      return errExc;
    }
  }

  /**
   * Goes through the response from GitHub to find the latest version up until the date.
   *
   * @param response The HTTP Response object.
   * @param date The date in String format.
   * @return The sha of the commit that is the latest up until the date.
   */
  private String findLatestCommitUntilDate(String response, String date) {
    logger.entering(CLASS_NAME, "findLatestCommitUntilDate");
    JSONArray jsonArr = JsonUtils.parseJsonArray(response);
    Iterator<JSONObject> iterator = jsonArr.iterator();
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    Date limit = null;
    try {
      limit = formatter.parse(date);
    } catch (Exception e) {
      logger.severe("Parse exception: " + e.getMessage());
      return null;
    }

    Date latest = new Date(Long.MIN_VALUE);
    String sha = null;

    while (iterator.hasNext()) {
      JSONObject obj = iterator.next();
      JSONObject commit = (JSONObject) obj.get("commit");
      JSONObject committer = (JSONObject) commit.get("committer");

      String commitDateString = committer.get("date").toString();
      Date commitDate = null;
      try {
        commitDate = formatter.parse(commitDateString);
      } catch (Exception e) {
        logger.severe("Parse exception: " + e.getMessage());
        return null;
      }

      if (commitDate.compareTo(latest) > 0
          && (commitDate.compareTo(limit) < 0 || commitDate.compareTo(limit) == 0)) {
        latest = commitDate;
        sha = obj.get("sha").toString();
      }
    }
    logger.exiting(CLASS_NAME, "findLatestCommitUntilDate", sha);
    return sha;
  }
}
