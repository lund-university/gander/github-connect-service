package pur.service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.text.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import spark.Request;
import spark.Response;

/** Class for handling the pull requests received from GitHub */
class PullRequestService {
  private static final String GIT_OWNER = System.getenv("GIT_OWNER") != null ? System.getenv("GIT_OWNER") : "granttitus";
  private static final String GIT_REPO = System.getenv("GIT_REPO") != null ? System.getenv("GIT_REPO") : "FlappyBird";
  private static final String GIT_ADDRESS = System.getenv("GIT_ADDRESS") != null ? System.getenv("GIT_ADDRESS") : "https://api.github.com";
  private static final String GIT_REPO_PATH = "repos";
  private static final String GIT_PULLS = "pulls";
  private static final String GIT_COMMENTS = "comments";
  private static final String CLASS_NAME = "PullRequestService";
  private static final String NBR_OF_PRS = System.getenv("NBR_OF_PRS") != null ? System.getenv("NBR_OF_PRS") : "100";

  private HashMap<String, String> pullRequestCache = new HashMap<String, String>();
  Logger logger;

  /** @param logger the Logger object used to debug log */
  public PullRequestService(Logger log) {
    this.logger = log;
    logger.info("PullRequestService started");
  }

  /**
   * Retrieves the commits from a pull request from GitHub.
   *
   * @param pullNumber The pull request number.
   * @return A JSON Array containing the commits from the specific pull request.
   */
  private JSONArray addPullRequestCommits(String owner, String repo, String pullNumber) {
    String url =
        String.format(
            "%s/%s/%s/%s/%s/%s/commits",
            GIT_ADDRESS, GIT_REPO_PATH, owner, repo, GIT_PULLS, pullNumber);

    try {
      HttpURLConnection c = HttpConnectionUtils.setGithubConnection(url, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonString = JsonUtils.processGithubResponse(c.getInputStream());

        return processCommits(jsonString);
      } else {

        String errGit =
            String.format(
                "%d: Error while geting from git %s", c.getResponseCode(), c.getResponseMessage());
        logger.severe(errGit);
      }
    } catch (IOException e) {
      String errPath = String.format("Invalid path to git repo: %d", e.getMessage());
      logger.severe(errPath);
    }

    return null;
  }

  /**
   * Takes the JSON String received from GitHub and converts it to a list of Commits.
   *
   * @param jsonString The response from GitHub.
   * @return A JSON Array containing the commits.
   */
  private JSONArray processCommits(String jsonString) {
    JSONArray arrayToParse = JsonUtils.parseJsonArray(jsonString);
    JSONArray response = new JSONArray();
    Iterator<JSONObject> iterator = arrayToParse.iterator();
    while (iterator.hasNext()) {
      JSONObject commit = new JSONObject();
      JSONObject obj = iterator.next();
      JSONObject commitToParse = (JSONObject) obj.get("commit");
      commit.put("sha", obj.get("sha"));
      if (commitToParse != null) {
        commit.put("message", commitToParse.get("message"));
        response.add(commit);
      }
    }
    return response;
  }

  /**
   * Retrieves the pull request with the specified pull request number.
   *
   * @param req The Request object. Should contain the param ":pull_number".
   * @param res The Response object.
   * @return The found pull request as a JSON String.
   */
  public String getPullRequest(Request req, Response res) {
    String pullNumber = req.params(":pull_number");

    if (pullRequestCache.containsKey(pullNumber)) {
      res.status(HttpURLConnection.HTTP_OK);
      return pullRequestCache.get(pullNumber);
    }

    String url =
        String.format(
            "%s/%s/%s/%s/%s/%s",
            GIT_ADDRESS, GIT_REPO_PATH, GIT_OWNER, GIT_REPO, GIT_PULLS, pullNumber);

    try {
      HttpURLConnection c = HttpConnectionUtils.setGithubConnection(url, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonString = JsonUtils.processGithubResponse(c.getInputStream());
        res.status(HttpURLConnection.HTTP_OK);
        JSONObject jsonResponse = processPullRequest(jsonString);
        jsonResponse.put("commits", addPullRequestCommits(GIT_OWNER, GIT_REPO, pullNumber));

        String returnString = jsonResponse.toString();
        pullRequestCache.put(pullNumber, returnString);
        return returnString;
      } else {
        res.status(c.getResponseCode());
        String errGit =
            String.format(
                "%d: Error while geting from git %s", c.getResponseCode(), c.getResponseMessage());
        logger.severe(errGit);
        return errGit;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errPath = String.format("Invalid path to git repo: %d", e.getMessage());
      logger.severe(errPath);
      return errPath;
    }
  }

  public String getSpecificPullRequest(Request req, Response res) {
    String owner = req.params(":owner");
    String repo = req.params(":repo");
    String pullNumber = req.params(":pull_number");

    String url =
        String.format(
            "%s/%s/%s/%s/%s/%s",
            GIT_ADDRESS, GIT_REPO_PATH, owner, repo, GIT_PULLS, pullNumber);

    try {
      HttpURLConnection c = HttpConnectionUtils.setGithubConnection(url, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonString = JsonUtils.processGithubResponse(c.getInputStream());
        res.status(HttpURLConnection.HTTP_OK);
        JSONObject jsonResponse = processPullRequest(jsonString);
        jsonResponse.put("commits", addPullRequestCommits(owner, repo, pullNumber));

        String returnString = jsonResponse.toString();
        return returnString;
      } else {
        res.status(c.getResponseCode());
        String errGit =
            String.format(
                "%d: Error while geting from git %s", c.getResponseCode(), c.getResponseMessage());
        logger.severe(errGit);
        return errGit;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errPath = String.format("Invalid path to git repo: %d", e.getMessage());
      logger.severe(errPath);
      return errPath;
    }
}

  /**
   * Takes the JSON String received from GitHub and converts it to a JSON Object containing the pull
   * request.
   *
   * @param jsonString The response from GitHub.
   * @return A JSON Object containing the pull request.
   */
  private JSONObject processPullRequest(String jsonString) {
    JSONObject response = new JSONObject();
    try {

      JSONObject arrayToParse = JsonUtils.parseJSONObject(jsonString);

      response.put("id", arrayToParse.get("id"));
      response.put("title", arrayToParse.get("title"));
      response.put("number", arrayToParse.get("number"));
      response.put("state", arrayToParse.get("state"));
      response.put("body", arrayToParse.get("body"));
      response.put("merged_at", arrayToParse.get("merged_at"));
      response.put("created_at", arrayToParse.get("created_at"));
      JSONArray responseLabels = (JSONArray) arrayToParse.get("labels");
      JSONArray labels = new JSONArray();
      Iterator<JSONObject> iterator = responseLabels.iterator();
      while (iterator.hasNext()) {
        JSONObject obj = iterator.next();
        JSONObject label = new JSONObject();
        label.put("name", obj.get("name"));
        label.put("description", obj.get("description"));
        label.put("color", obj.get("color"));
        labels.add(label);
      }
      response.put("labels", labels);
      JSONObject milestone = (JSONObject) arrayToParse.get("milestone");
      if (milestone != null) {
        response.put("milestone_title", milestone.get("title"));
      }

    } catch (Exception e) {
      System.out.println(e);
    }
    return response;
  }

  /**
   * Retrieves the list of all pull requests in the repo.
   *
   * @param req The Request object.
   * @param res The Response object.
   * @return The found list of pull requests as a JSON String.
   */
  public String getPullRequestList(Request req, Response res) {
    logger.entering(CLASS_NAME, "getPullRequestList");

    int numberOfPRs;
    try {
      numberOfPRs = Math.min(Integer.parseInt(NBR_OF_PRS), 100);
    } catch (NumberFormatException ex) {
      numberOfPRs = 100;
    }

    String url =
        String.format(
            "%s/%s/%s/%s/%s?state=all&per_page=%s", GIT_ADDRESS, GIT_REPO_PATH, GIT_OWNER, GIT_REPO, GIT_PULLS, numberOfPRs);

    try {
      HttpURLConnection c = HttpConnectionUtils.setGithubConnection(url, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        String jsonString = JsonUtils.processGithubResponse(c.getInputStream());
        res.status(HttpURLConnection.HTTP_OK);

        return processPullRequestList(jsonString);
      } else {
        res.status(c.getResponseCode());
        String errGit =
            String.format(
                "%d: Error while geting from git %s", c.getResponseCode(), c.getResponseMessage());
        logger.severe(errGit);
        return errGit;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errPath = String.format("Invalid path to git repo: %d", e.getMessage());
      logger.severe(errPath);
      return errPath;
    }
  }

  /**
   * Takes the JSON String received from GitHub and converts it to a list of pull requests.
   *
   * @param jsonString The response from GitHub.
   * @return A JSON Array containing the pull requests.
   */
  private String processPullRequestList(String jsonString) {
    try {
      JSONArray response = new JSONArray();
      JSONArray pullRequestList = JsonUtils.parseJsonArray(jsonString);
      Iterator<JSONObject> iterator = pullRequestList.iterator();
      while (iterator.hasNext()) {
        JSONObject obj = iterator.next();
        JSONObject responseObj = new JSONObject();
        long id = (long) obj.get("id");
        long number = (long) obj.get("number");
        String body = (String) obj.get("body");
        String state = (String) obj.get("state");
        String createdAt = (String) obj.get("created_at");
        String mergedAt = (String) obj.get("merged_at");
        String title = (String) obj.get("title");
        responseObj.put("id", id);
        responseObj.put("number", number);
        responseObj.put("body", body);
        responseObj.put("state", state);
        responseObj.put("merged_at", mergedAt);
        responseObj.put("created_at", createdAt);
        responseObj.put("title", title);

        response.add(responseObj);
      }

      return response.toString();

    } catch (Exception e) {
      System.out.println(e);
      return e.getMessage();
    }
  }

  /**
   * Retrieves the list of all comments in the pull request.
   *
   * @param req The Request object. Should contain the param ":pull_number"
   * @param res The Response object.
   * @return The found list of comments as a JSON String.
   */
  public String getCommentList(Request req, Response res) {
    String pullNumber = req.params(":pull_number");
    String url =
        String.format(
            "%s/%s/%s/%s/%s/%s/%s",
            GIT_ADDRESS, GIT_REPO_PATH, GIT_OWNER, GIT_REPO, GIT_PULLS, pullNumber, GIT_COMMENTS);
    logger.entering(CLASS_NAME, "getCommentList", url);
    try {
      HttpURLConnection c = HttpConnectionUtils.setGithubConnection(url, "GET");

      if (c.getResponseCode() == HttpURLConnection.HTTP_OK) {
        logger.fine("Response Code OK");
        String jsonString = JsonUtils.processGithubResponse(c.getInputStream());
        logger.fine("JSON String: " + jsonString);
        res.status(HttpURLConnection.HTTP_OK);
        logger.exiting(CLASS_NAME, "getCommentList");
        return processCommentList(jsonString);
      } else {
        res.status(c.getResponseCode());
        String errGit =
            String.format(
                "%d: Error while geting comments from git %s",
                c.getResponseCode(), c.getResponseMessage());
        logger.severe(errGit);
        return errGit;
      }
    } catch (IOException e) {
      res.status(HttpURLConnection.HTTP_BAD_REQUEST);
      String errPath =
          String.format("Invalid path to comments for the repo pull request: %d", e.getMessage());
      logger.severe(errPath);
      return errPath;
    }
  }

  /**
   * Takes the JSON String received from GitHub and converts it to a list of comments.
   *
   * @param jsonString The response from GitHub.
   * @return A JSON Array containing the comments.
   */
  private String processCommentList(String jsonString) {
    logger.entering(CLASS_NAME, "processCommentList", jsonString);
    try {
      JSONArray response = new JSONArray();
      JSONArray commentList = JsonUtils.parseJsonArray(jsonString);
      for (Object obj : commentList) {
        JSONObject jsonobj = (JSONObject) obj;
        JSONObject responseObj = new JSONObject();
        long id = (long) jsonobj.get("id");
        long number;
        if(jsonobj.get("pull_request_review_id") == null || jsonobj.get("pull_request_review_id") == "null" ) {
          number = 0;          
        } else {
          number = (long) jsonobj.get("pull_request_review_id");
        }
        String body = (String) jsonobj.get("body");
        String diff_hunk = (String) jsonobj.get("diff_hunk");
        String createdAt = (String) jsonobj.get("created_at");
        String mergedAt = (String) jsonobj.get("updated_at");
        String file = (String) jsonobj.get("path");
        //TEST GETTING LINE FROM JSON RESPONSE
        //The value "line" has been in the response the whole time. It's null when the file is out of date, ie. put in gitignore.
        System.out.println(jsonobj.get("line"));
        long line;
        if(jsonobj.get("line") == null) {
          line = -1;
        } else {
          line = (long) jsonobj.get("line");
        }

        responseObj.put("line",line);
        responseObj.put("id", id);
        responseObj.put("number", number);
        responseObj.put("body", body);
        responseObj.put("diff_hunk", diff_hunk);
        responseObj.put("updated_at", mergedAt);
        responseObj.put("created_at", createdAt);
        responseObj.put("file", file);

        responseObj.put("user", (String) ((JSONObject) jsonobj.get("user")).get("login"));

        if (jsonobj.get("in_reply_to_id") != null) {
          responseObj.put("is_a_reply", "true");
          long in_reply_to_id = (long) jsonobj.get("in_reply_to_id");
          responseObj.put("reply_to_id", in_reply_to_id);
        } else {
          responseObj.put("is_a_reply", "false");
        }
        response.add(responseObj);
      }
      logger.exiting(CLASS_NAME, "getCommentList", response.toString());
      return response.toString();

    } catch (Exception e) {
      logger.severe("Error while trying to parse Comment list: " + e.getMessage());
      return e.getMessage();
    }
  }
}
