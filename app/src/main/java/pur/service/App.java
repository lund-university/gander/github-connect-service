package pur.service;

import static spark.Spark.after;
import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.put;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import spark.Filter;

public class App {
  private static final int DEFAULT_PORT = 8002;
  private static String logsFolderPath;

  /** Starts the server. */
  public static void main(String[] args) {
    int port = args.length > 0 ? parsePortArgument(args[0]) : DEFAULT_PORT;
    port(port);
    Logger logger = setupLogger(port);
    logger.info("-- STARTING PUR SERVICE ON PORT " + port + " --");
    PingService s = new PingService(logger);
    PurService pur = new PurService(logger);
    GitService git = new GitService(pur, logger);
    PullRequestService pullRequest = new PullRequestService(logger);

    // Adding placeholders for testing in the client
    // pur.addPlaceholders();

    after(
        (Filter)
            (request, response) -> {
              response.header("Access-Control-Allow-Origin", "*");
              response.header("Access-Control-Allow-Methods", "GET");
            });
    get("/ping", (req, res) -> s.ping(req, res));
    post("/pur", (req, res) -> pur.postPur(req, res));
    get("/pur", (req, res) -> pur.getPurs(req, res));
    get("/pur/commits", (req, res) -> git.getCommits(req, res));
    get("/pur/commits/:ref", (req, res) -> git.getCommitContent(req, res));
    get("/pur/:name", (req, res) -> git.getContent(req, res));
    put("/pur/:name", (req, res) -> pur.putContent(req, res));
    // get("/pur/:name/:sha", (req, res) -> pur.getContentBySha(req, res));
    // put("/pur/:name/:sha", (req, res) -> pur.putContentBySha(req, res));
    get("/pur/:name/:nestedFile", (req, res) -> git.getContent(req, res));
    put("/pur/:name/:nestedFile", (req, res) -> pur.putContent(req, res));

    get("/git", (req, res) -> git.getFilesInGitRepo(req, res));
    get("/git/limit", (req, res) -> git.getRateLimit(req, res));
    get("/git/pulls", (req, res) -> pullRequest.getPullRequestList(req, res));
    get("/git/pulls/:pull_number", (req, res) -> pullRequest.getPullRequest(req, res));
    get("/git/pulls/:pull_number/comments", (req, res) -> pullRequest.getCommentList(req, res));
    get("/git/pulls/:owner/:repo/:pull_number", (req, res) -> pullRequest.getSpecificPullRequest(req, res));
    get("/git/:name/:date", (req, res) -> git.getLatestVersionOfFileUntilDate(req, res));
  }

  /**
   * Parses the incoming argument that should be containing the port number.
   *
   * @param portNbr the port number as a String
   * @return the port number as an int
   */
  public static int parsePortArgument(String portNbr) {
    try {
      return Integer.parseInt(portNbr);
    } catch (NumberFormatException e) {
      System.out.println(
          "Could not resolve the argument to a port number, using the default port number "
              + DEFAULT_PORT);
      return DEFAULT_PORT;
    }
  }

  /**
   * Initializes a Logger object. Creates a new entry in the log-folder.
   *
   * @param port port number the service is active on
   * @return the Logger object
   */
  public static Logger setupLogger(int port) {
    createLogFolder();
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
    Logger logger = Logger.getLogger(App.class.getName());
    FileHandler fh;
    try {
      fh =
          new FileHandler(
              logsFolderPath + "/Log_" + dtf.format(LocalDateTime.now()) + "_port" + port + ".log",
              true);
      fh.setFormatter(new SimpleFormatter());
      logger.addHandler(fh);
      // Change the line below for more/less logging info
      logger.setLevel(Level.ALL);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return logger;
  }

  /** Creates a folder for the logs, if it doesn't already exist. */
  private static void createLogFolder() {
    logsFolderPath = Paths.get("logs").toAbsolutePath().toString();
    try {
      Files.createDirectories(Paths.get(logsFolderPath));
    } catch (IOException e) {
      System.out.println("Error while creating the logs directory. ");
    }
  }
}
