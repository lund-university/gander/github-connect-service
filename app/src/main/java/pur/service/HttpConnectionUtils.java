package pur.service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.*;
import java.util.Base64;

/**
 * A utils class for setting up connections and authentications to GitHub. For example:
 *
 * <p>A HttpUrlConnection without headers.
 *
 * <p>A HttpUrlConnection with "application/json" as content-type.
 *
 * <p>A Connection with the GitHub Authentication token.
 */
class HttpConnectionUtils {

  /**
   * Used for establishing connection with content-type header set to application/json.
   *
   * @param urlString the URL String
   * @param method the HTTP request type, for example "GET".
   * @return HttpUrlConnection object
   * @throws IOException
   */
  public static HttpURLConnection setConnection(String urlString, String method)
      throws IOException {
    HttpURLConnection c = HttpConnectionUtils.setConnectionNoHeaders(urlString, method);
    c.setRequestProperty("Content-Type", "application/json");
    c.setRequestProperty("Accept", "application/json");
    return c;
  }

  /**
   * Used for establishing connection with no set headers
   *
   * @param urlString the URL String
   * @param method the HTTP request type, for example "GET".
   * @return HttpUrlConnection object
   * @throws IOException
   */
  public static HttpURLConnection setConnectionNoHeaders(String urlString, String method)
      throws IOException {
    URL url = new URL(urlString);
    HttpURLConnection c = (HttpURLConnection) url.openConnection();
    c.setRequestMethod(method);
    return c;
  }

  /**
   * Used for establishing connection with content-type header set to application/json and GitHub
   * authentication token provided. Used when communicating with GitHub.
   *
   * @param urlString the URL String
   * @param method the HTTP request type, for example "GET".
   * @return HttpUrlConnection object
   * @throws IOException
   */
  public static HttpURLConnection setGithubConnection(String urlString, String method)
      throws IOException {
    URL url = new URL(urlString);
    HttpURLConnection c = (HttpURLConnection) url.openConnection();
    c.setRequestMethod(method);
    c.setRequestProperty("Accept", "application/vnd.github.v3+json");
    c.setRequestProperty("Authorization", getBasicAuth());
    return c;
  }

  /**
   * Reads the credentials.txt file and retrieves the Auth token. (See more information about the
   * credentials.txt file in README.md)
   *
   * @return the Authentication token as requested by GitHub
   */
  private static String getBasicAuth() {
    try {
      Path credentialsPath = FileSystems.getDefault().getPath("credentials.txt").toAbsolutePath();

      String userCredentials = new String(Files.readAllBytes(credentialsPath));

      String basicAuth =
          "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));

      return basicAuth;

    } catch (Exception e) {
      System.out.println("Error reading credentials.txt file:  " + e);
      return null;
    }
  }
}
