package pur.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/** A utils class for handling JSON Objects */
public class JsonUtils {

  /**
   * Parses the response from a request as a JSON String. It reads the string from the InputStream
   * and produces the string by parsing it as a JSON Object and returning the JSON String
   * representation.
   *
   * @param response JSON Object response from a request.
   * @return String containing the JSON String representation of the response.
   */
  public static String processResponse(InputStream res) throws IOException {
    BufferedReader in = new BufferedReader(new InputStreamReader(res));
    String inputLine;
    StringBuffer resp = new StringBuffer();

    while ((inputLine = in.readLine()) != null) {
      resp.append(inputLine);
    }

    in.close();
    try {
      JSONParser jsonParser = new JSONParser();
      JSONObject jsonObject = (JSONObject) jsonParser.parse(resp.toString());
      return jsonObject.toString();

    } catch (Exception e) {
      return String.format("JSON Parser exception: %d", e.getMessage());
    }
  }

  /**
   * Parses the response from a GitHub request as a String.
   *
   * @param response Response from a request.
   * @return String containing the GitHub response.
   */
  public static String processGithubResponse(InputStream res) throws IOException {
    BufferedReader in = new BufferedReader(new InputStreamReader(res));
    String inputLine;
    StringBuffer resp = new StringBuffer();

    while ((inputLine = in.readLine()) != null) {
      resp.append(inputLine);
    }
    in.close();
    return resp.toString();
  }

  /**
   * Parses the response as a JSON Array.
   *
   * @param response Response String from a request.
   * @return JSON Array.
   */
  public static JSONArray parseJsonArray(String body) {

    JSONParser parser = new JSONParser();
    JSONArray json = new JSONArray();
    try {
      return json = (JSONArray) parser.parse(body);

    } catch (Exception e) {
      return null;
    }
  }

  /**
   * Parses the response as a JSON Object.
   *
   * @param response Response from a request.
   * @return JSON Object.
   */
  public static JSONObject parseJSONObject(String body) {

    JSONParser parser = new JSONParser();
    JSONObject json = new JSONObject();
    try {
      return json = (JSONObject) parser.parse(body);

    } catch (Exception e) {
      return null;
    }
  }

  /**
   * Takes the input, a list of PurContent in a folder, and turns them into a JSON Array.
   *
   * @param files The List och PurContent.
   * @return A JSON Array.
   */
  public static JSONArray makeFolderFileArray(ArrayList<PurContent> files) {
    JSONArray json = new JSONArray();
    try {
      for (PurContent content : files) {
        JSONObject obj = new JSONObject();
        obj.put("name", content.getName().toString());
        obj.put("type", content.getType());
        if (content.isFile()) {
          obj.put("content", ((PurFile) content).getContent());
        } else {
          obj.put("folderFiles", makeFolderFileArray(((PurFolder) content).getFolderFiles()));
        }
        json.add(obj);
      }
      return json;
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * Creates the Commit information JSON Object from the string containing the commit information
   * from GitHub. Since we are not interested in all of the information received by GitHub, we are
   * picking the important ones out.
   *
   * @param jsonString Containing the commit from GitHub.
   * @return A JSON Object containing the important Commit information.
   */
  public static JSONObject createCommitInfo(String jsonString) {
    JSONObject obj = JsonUtils.parseJSONObject(jsonString);

    JSONArray commitArray = new JSONArray();
    JSONObject commitObject = new JSONObject();

    commitObject.put("sha", obj.get("sha"));
    JSONObject commit = (JSONObject) obj.get("commit");
    commitObject.put("message", commit.get("message"));
    JSONObject author = (JSONObject) commit.get("author");
    commitObject.put("authorName", author.get("name"));
    commitObject.put("date", author.get("date"));

    JSONArray fileArray = (JSONArray) obj.get("files");

    Iterator<JSONObject> iterator = fileArray.iterator();

    while (iterator.hasNext()) {

      JSONObject file = iterator.next();
      JSONObject newObj = new JSONObject();
      newObj.put("filename", file.get("filename"));
      newObj.put("additions", file.get("additions"));
      newObj.put("deletions", file.get("deletions"));

      commitArray.add(newObj);
    }

    commitObject.put("files", commitArray);

    return commitObject;
  }

  /**
   * Creates a list of all of the commits in a JSON Array. This is done by parsing the input string
   * as a JSON Array and looping over each JSON Object in the Array, getting the relevant
   * information, and returning the new JSON Array.
   *
   * @param s The input JSON Array as a String.
   * @return A JSON Array.
   */
  public static JSONArray createCommitList(String s) {

    JSONParser jsonParser = new JSONParser();
    JSONArray result = new JSONArray();
    JSONArray jsonArr = JsonUtils.parseJsonArray(s);
    Iterator<JSONObject> iterator = jsonArr.iterator();
    while (iterator.hasNext()) {

      JSONObject obj = iterator.next();

      JSONObject newObj = new JSONObject();
      newObj.put("sha", obj.get("sha"));
      JSONObject commit = (JSONObject) obj.get("commit");
      newObj.put("message", commit.get("message"));

      JSONObject committer = (JSONObject) commit.get("committer");

      if (committer != null) {
        newObj.put("name", committer.get("name"));
        newObj.put("date", committer.get("date"));
      }
      result.add(newObj);
    }
    return result;
  }
}
