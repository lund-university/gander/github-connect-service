package pur.service;

/** A Utils class for */
public class Utils {

  /**
   * Removes all of the tabs representation with the indent in the file that we want.
   *
   * @param content The content which we want to clear the tab signs in.
   * @return The content without tab signs.
   */
  static String removeAllTabs(String content) {

    return content.replaceAll("\\t", "    ");
  }
}
