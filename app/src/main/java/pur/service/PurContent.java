package pur.service;

import java.util.HashSet;

/** An abstract class for the PurFiles and PurFolders. */
abstract class PurContent {
  private String name;
  private HashSet<PurCommit> commits;

  /**
   * Retrieves the name of the PurContent.
   *
   * @return the name of the PurContent.
   */
  public abstract String getName();

  /**
   * Gets the commits that the PurContent is a part of.
   *
   * @return A Set of PurCommits.
   */
  public abstract HashSet<PurCommit> getCommits();

  /**
   * Gets the type of the PurContent.
   *
   * @return The type of PurContent, for example PurFile.
   */
  public abstract String getType();

  /**
   * A checker for seeing if the PurContent is a file ot not.
   *
   * @return True if it is a file, False if not.
   */
  public abstract boolean isFile();
}
