package pur.service;

import java.util.logging.Logger;
import org.json.simple.JSONObject;
import spark.Request;
import spark.Response;

/** Class used to test the connection to the Pur Service */
class PingService {
  private static final String CLASS_NAME = PingService.class.getName();
  private Logger logger;

  /** @param logger the Logger object used to debug log */
  public PingService(Logger log) {
    this.logger = log;
    logger.info("PingService started");
  }

  /**
   * Returns a "pong"-message to the ping-request.
   *
   * @param req The Request object.
   * @param res The Response object.
   * @return A JSON String containing the message "pong".
   */
  public String ping(Request req, Response res) {
    logger.entering(CLASS_NAME, "ping");
    JSONObject obj = new JSONObject();
    obj.put("message", "pong");
    String jsonText = obj.toString();
    res.body(jsonText);
    res.status(200);
    logger.exiting(CLASS_NAME, "ping", jsonText);
    return jsonText;
  }
}
