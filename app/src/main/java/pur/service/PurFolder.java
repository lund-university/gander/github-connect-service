package pur.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/** A Class for representing folders */
public class PurFolder extends PurContent {
  private String name;
  private String sha;
  private HashSet<PurCommit> commits;
  private ArrayList<PurContent> folderFiles;

  /**
   * Constructor for when we know the Sha number of the folder.
   *
   * @param name The name of the folder.
   * @param sha The sha number of the folder.
   */
  public PurFolder(String name, String sha) {
    this.name = name;
    this.sha = sha;
    this.commits = new HashSet<PurCommit>();
    this.folderFiles = new ArrayList<PurContent>();
  }

  /**
   * Constructor for when we only know the folder's name.
   *
   * @param name The name of the folder.
   */
  public PurFolder(String name) {
    this.name = name;
    this.commits = new HashSet<PurCommit>();
    this.folderFiles = new ArrayList<PurContent>();
  }

  /**
   * Retrieves the name of the folder.
   *
   * @return the name of the folder.
   */
  public String getName() {
    return this.name;
  }

  /**
   * Retrieves the sha of the folder.
   *
   * @return the sha of the folder.
   */
  public String getSha() {
    return this.sha;
  }

  /**
   * Sets the sha of the folder.
   *
   * @param sha The String value of the sha number.
   */
  public void setSha(String sha) {
    this.sha = sha;
  }

  /**
   * Gets the commits that the folder is a part of.
   *
   * @return A Set of PurCommits.
   */
  public HashSet<PurCommit> getCommits() {
    return this.commits;
  }

  /**
   * Goes through the lists of commits that the folder has been a part of and retrieves the one that
   * has a sha matching the one we search for.
   *
   * @param sha The sha of the commit we are looking for.
   * @return The PurCommit with the matching sha.
   */
  public PurCommit getCommit(String sha) {
    Iterator<PurCommit> iterator = commits.iterator();
    while (iterator.hasNext()) {
      PurCommit commit = iterator.next();
      if (commit.getSha().equals(sha)) {
        return commit;
      }
    }
    return null;
  }

  /**
   * Adds to the folder content.
   *
   * @param content The new folder content.
   */
  public void addFolderFile(PurContent content) {
    folderFiles.add(content);
  }

  /**
   * Adds all of the content in a List to the folder content.
   *
   * @param contents The list of folder contents.
   */
  public void appendFolderFiles(ArrayList<PurContent> contents) {
    folderFiles.addAll(contents);
  }

  /**
   * Retrieves all of the content for the folder.
   *
   * @return A List of the content of the folder.
   */
  public ArrayList<PurContent> getFolderFiles() {
    return folderFiles;
  }

  /**
   * Checks if the folder contains a file or folder with the name.
   *
   * @param name The name of the content.
   * @return True if it is inside the folder, False if not.
   */
  public boolean containsFile(String name) {
    for (PurContent content : folderFiles) {
      if (content.getName().equals(name)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Gets the type of the folder.
   *
   * @return "dir".
   */
  public String getType() {
    return "dir";
  }

  /**
   * A checker for seeing if the folder is a file ot not.
   *
   * @return False.
   */
  public boolean isFile() {
    return false;
  }
}
