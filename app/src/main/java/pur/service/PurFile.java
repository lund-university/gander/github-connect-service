package pur.service;

import java.util.HashSet;
import java.util.Iterator;
import org.json.simple.JSONObject;

/** A Class for representing files */
public class PurFile extends PurContent {
  private String name;
  private String content;
  private HashSet<PurCommit> commits;

  /**
   * Constructor for the PurFile
   *
   * @param name The name of the file.
   * @param sha The sha of the file.
   * @param content The content of the file.
   */
  public PurFile(String name, String sha, String content) {
    this.name = name;
    this.content = content;
    this.commits = new HashSet<PurCommit>();
    this.commits.add(new PurCommit(sha, content));
  }

  /** Constructor for testing without providing content */
  public PurFile(String name, String sha) {
    this.name = name;
    this.content = "";
    this.commits = new HashSet<PurCommit>();
    this.commits.add(new PurCommit(sha, "content for " + sha));
  }

  /**
   * Gets the type of the file.
   *
   * @return "file".
   */
  public String getType() {
    return "file";
  }

  /**
   * Gets the content of the file.
   *
   * @return The String content of the file.
   */
  public String getContent() {
    return content;
  }

  /**
   * Sets the content of the file to newContent.
   *
   * @param newContent The new content String.
   */
  public void setContent(String newContent) {
    content = newContent;
  }

  /**
   * Retrieves the name of the file.
   *
   * @return the name of the file.
   */
  public String getName() {
    return this.name;
  }

  /**
   * Gets the commits that the file is a part of.
   *
   * @return A Set of PurCommits.
   */
  public HashSet<PurCommit> getCommits() {
    return this.commits;
  }

  /**
   * Goes through the lists of commits that the file has been a part of and retrieves the one that
   * has a sha matching the one we search for.
   *
   * @param sha The sha of the commit we are looking for.
   * @return The PurCommit with the matching sha.
   */
  public PurCommit getCommit(String sha) {
    Iterator<PurCommit> iterator = commits.iterator();
    while (iterator.hasNext()) {
      PurCommit commit = iterator.next();
      if (commit.getSha().equals(sha)) {
        return commit;
      }
    }
    return null;
  }

  /**
   * Adds a commit to the list of commits that the file has been a part of.
   *
   * @param sha The sha of the commit.
   * @param content The content that the file had in that specific commit.
   */
  public void addCommit(String sha, String content) {
    commits.add(new PurCommit(sha, content));
  }

  @Override
  public boolean equals(Object o) {

    if (o == this) {
      return true;
    }
    if (!(o instanceof PurFile)) {
      return false;
    }
    PurFile f = (PurFile) o;

    return f.getName().equals(name);
  }

  public boolean isFile() {
    return true;
  }

  @Override
  public String toString() {
    return name;
  }

  /**
   * A JSON Object representation of the file.
   *
   * @return A JSON Object representation of the file.
   */
  public JSONObject toJSON() {
    String res = "{ \"name\": \"" + name + "\",\"type\": \"file\"}";

    return JsonUtils.parseJSONObject(res);
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }
}
