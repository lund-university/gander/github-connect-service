package pur.service;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.HashSet;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import spark.Request;
import spark.Response;

/** Class for handling the storing and retrieval of files and directories */
class PurService {
  private HashSet<PurContent> purSet = new HashSet<PurContent>();
  private Logger logger;

  private static final String CLASS_NAME = PurService.class.getName();
  private static final String PUR_FILE = "file";
  private static final String PUR_FOLDER = "dir";

  /** @param logger the Logger object used to debug log */
  public PurService(Logger log) {
    this.logger = log;
    logger.info("PurService started");
  }

  /**
   * Adds the PurContent to the local storage of Pur Service.
   *
   * @param content The content we want to add.
   */
  public void addPur(PurContent content) {
    logger.entering(CLASS_NAME, "addPur", content);
    purSet.add(content);
    logger.exiting(CLASS_NAME, "addPur");
  }

  // Adding placeholders for testing in the client
  public void addPlaceholders() {
    logger.entering(CLASS_NAME, "addPlaceholders");
    purSet.add(
        new PurFile(
            "Pygmy",
            "e9611923bd9467c1c6e71f4efd110d101c23d2b6",
            "class Simple{\n"
                + "  public static void main(String args[]){\n"
                + "   System.out.println(\"Hello Java\");\n"
                + "  }\n"
                + "} "));
    purSet.add(
        new PurFile(
            "Gander",
            "ab8f308414a0cb05c129642a6595f15b0f377e0c",
            "class Simple2{\n"
                + "  public static void main(String args[]){\n"
                + "      String hi = a();\n"
                + "  }\n"
                + "  public static String a(){\n"
                + "      String ret = function2();\n"
                + "      return ret;\n"
                + "  }\n"
                + "  public static String function2(){\n"
                + "      return \"Hello\";\n"
                + "  }\n"
                + "}"));
    purSet.toArray(new PurFile[0])[1].addCommit(
        "ab8f308414a0cb05c129642a6595f15b0f377e01",
        "class Simple2{\n"
            + "  public static void main(String args[]){\n"
            + "      String hi = function2();\n"
            + "  }\n"
            + "  public static String a(){\n"
            + "      String ret = function2();\n"
            + "      return ret;\n"
            + "  }\n"
            + "  public static String function2(){\n"
            + "      return \"Hello\";\n"
            + "  }\n"
            + "}");

    logger.exiting(CLASS_NAME, "addPlaceholders");
  }

  /**
   * Adds the incoming JSON Object as a file in the local storage.
   *
   * @param req The Request object.
   * @param res The Response object.
   * @return The service response as a JSON String.
   */
  public String postPur(Request req, Response res) {
    logger.entering(CLASS_NAME, "postPur");
    JSONObject json = JsonUtils.parseJSONObject(req.body());
    if (json == null) {
      logger.exiting(CLASS_NAME, "postPur");
      return setBadFormat(res);
    }

    String name = (String) json.get("name");
    if (name == null) {
      String s1 = "error";
      String s2 = "no project found";
      json.put(s1, s2);
      logger.severe("error: no project found");
      String response = json.toString();
      res.body(response);
      res.status(400);
      logger.exiting(CLASS_NAME, "postPur", response);
      return response;
    }
    if (getPurContentByName(name) != null) {
      res.status(409);
      json.put("error", "resource already exists");
      logger.severe("error : resource already exists");
      String response = json.toString();
      res.body(response);
      logger.exiting(CLASS_NAME, "postPur", response);
      return response;
    }

    String content = (String) json.get("content");
    String sha;
    if (json.get("sha") != null) {
      sha = (String) json.get("sha");
    } else {
      sha = generateSha();
    }
    PurFile file;
    if (content == null) {
      file = new PurFile(name, sha);
    } else {
      file = new PurFile(name, sha, content);
    }
    purSet.add(file);
    JSONObject response = new JSONObject();
    response.put("result", name + " successfully added");
    response.put("addedResource", name);
    String responseString = response.toString();
    res.body(responseString);
    res.status(201);
    logger.exiting(CLASS_NAME, "postPur", responseString);
    return responseString;
  }

  /**
   * Retrieves the Pur Content that is stored on the Service.
   *
   * @param req The Request object.
   * @param res The Response object.
   * @return The found Pur Contents as a JSON String.
   */
  public String getPurs(Request req, Response res) {
    logger.entering(CLASS_NAME, "getPurs");
    JSONObject obj = new JSONObject();
    JSONArray list = new JSONArray();
    for (PurContent file : purSet) {
      JSONObject fileObj = new JSONObject();
      fileObj.put("name", file.getName());
      fileObj.put("commits", createShas(file.getCommits()));
      fileObj.put("type", file.getType());
      if (file.getType().equals(PUR_FILE)) {
        fileObj.put("content", ((PurFile) file).getContent());
      }
      list.add(fileObj);
    }
    obj.put("fileNames", list);
    String jsonText = obj.toString();
    res.body(jsonText);
    res.status(200);
    logger.exiting(CLASS_NAME, "getPurs");
    return jsonText;
  }

  /**
   * @param req The Request object. Should contain the param ":name".
   * @param res The Response object.
   * @return The service response as a JSON String.
   */
  public String putContent(Request req, Response res) {
    logger.entering(CLASS_NAME, "putContent");
    JSONObject json = JsonUtils.parseJSONObject(req.body());
    if (json == null) {
      logger.exiting(CLASS_NAME, "putContent");
      return setBadFormat(res);
    }
    String name = req.params(":name");
    JSONObject obj = new JSONObject();
    PurContent file = getPurContentByName(name);

    if (file == null) {
      res.status(404);
      obj.put("error", "file name not found");
      logger.severe("error: file name not found");
      res.body(obj.toString());
      return obj.toString();
    } else if (file.isFile()) {
      String content = (String) json.get("content");
      ((PurFile) file).setContent(content);
      ((PurFile) file).addCommit(generateSha(), content);
      obj.put("content", content);
    } else {
      obj.put("files", ((PurFolder) file).getFolderFiles().toString());
    }

    res.status(200);
    obj.put("status", "SUCCESS");
    obj.put("name", name);
    obj.put("commits", createShas(file.getCommits()));
    logger.exiting(CLASS_NAME, "putContent", obj.toString());
    return obj.toString();
  }

  /**
   * Adds the incoming PurContent to the speicifed commit sha.
   *
   * @param req The Request object. Should contain the param ":sha".
   * @param res The Response object.
   * @return The service response as a JSON String.
   */
  public String putContentBySha(Request req, Response res) {
    logger.entering(CLASS_NAME, "putContentBySha");
    JSONObject json = JsonUtils.parseJSONObject(req.body());
    if (json == null) {
      logger.exiting(CLASS_NAME, "putContentBySha");
      return setBadFormat(res);
    }
    String sha = req.params(":sha");
    JSONObject obj = new JSONObject();
    PurContent file = getPurContentBySha(sha);

    if (file == null || file.getType().equals(PUR_FOLDER)) {
      res.status(404);
      obj.put("error", "File commit not found");
      logger.severe("error: File commit not found");
      res.body(obj.toString());
      return obj.toString();
    }

    String name = file.getName();
    String content = (String) json.get("content");
    PurCommit commit = ((PurFile) file).getCommit(sha);
    commit.setContent(content);
    res.status(200);
    obj.put("status", "SUCCESS");
    obj.put("name", name);
    obj.put("commits", sha);
    obj.put("content", content);
    logger.exiting(CLASS_NAME, "putContentBySha", obj.toString());
    return obj.toString();
  }

  /**
   * Retrieves the PurContent with the specified sha.
   *
   * @param req The Request object. Should contain the param ":sha".
   * @param res The Response object.
   * @return The PurContent as a JSON String.
   */
  public String getContentBySha(Request req, Response res) {
    String sha = req.params(":sha");
    logger.entering(CLASS_NAME, "getContentBySha", sha);
    JSONObject obj = new JSONObject();
    PurContent file = getPurContentBySha(sha);
    obj.put("sha", sha);
    if (file == null) {

      res.status(404);
      obj.put("error", "File commit not found");
      logger.severe("error: File commit not found");
      res.body(obj.toString());
      return obj.toString();
    } else if (file.getType().equals(PUR_FILE)) {
      PurCommit commit = ((PurFile) file).getCommit(sha);
      obj.put("name", file.getName());
      obj.put("type", file.getType());
      obj.put("content", commit.getContent());
    } else if (file.getType().equals(PUR_FOLDER)) {
      obj.put("name", file.getName());
      obj.put("type", file.getType());
      obj.put("files", ((PurFolder) file).getFolderFiles().toString());
    }

    res.status(200);
    logger.exiting(CLASS_NAME, "getContentBySha", obj.toString());
    return obj.toString();
  }

  /**
   * Gets the specified PurContent by its name.
   *
   * @param name The name of the PurContent.
   * @return The PurContent object.
   */
  public PurContent getPurContentByName(String name) {
    logger.entering(CLASS_NAME, "getPurContentByName", name);
    for (PurContent f : purSet) {
      if (f.getName().equals(name)) {
        logger.exiting(CLASS_NAME, "getPurContentByName", f);
        return f;
      }
    }
    logger.exiting(CLASS_NAME, "getPurContentByName", null);
    return null;
  }

  /**
   * Gets the specified PurContent by its sha.
   *
   * @param sha The sha of the PurContent.
   * @return The PurContent object.
   */
  private PurContent getPurContentBySha(String sha) {
    logger.entering(CLASS_NAME, "getPurContentBySha", sha);
    for (PurContent f : purSet) {
      if (f.getType().equals(PUR_FILE)) {
        if (((PurFile) f).getCommit(sha) != null) {
          logger.exiting(CLASS_NAME, "getPurContentBySha", f);
          return f;
        }
      } else if (f.getType().equals(PUR_FOLDER)) {
        if (((PurFolder) f).getSha().equals(sha)) {
          logger.exiting(CLASS_NAME, "getPurContentBySha", f);
          return f;
        }
      }
    }
    logger.exiting(CLASS_NAME, "getPurContentBySha", null);
    return null;
  }

  /**
   * Creates a JSON Array from the Set of commits containing the sha numbers and their content.
   *
   * @param commits A Set of PurCommits.
   * @return A JSON Array containing a list of the shas and their content.
   */
  public JSONArray createShas(HashSet<PurCommit> commits) {
    logger.entering(CLASS_NAME, "createShas", commits);
    JSONArray list = new JSONArray();
    for (PurCommit commit : commits) {
      JSONObject shaObj = new JSONObject();
      shaObj.put("sha", commit.getSha());
      shaObj.put("content", commit.getContent());
      list.add(shaObj);
    }
    logger.exiting(CLASS_NAME, "createShas", list);
    return list;
  }

  /**
   * Generates a new sha for the files that has not been received from GitHub. This is only used
   * during testing.
   *
   * @return A sha String.
   */
  private String generateSha() {
    logger.entering(CLASS_NAME, "generateSha");
    SecureRandom secureRandom = new SecureRandom();
    byte[] token = new byte[16];
    secureRandom.nextBytes(token);
    logger.entering(CLASS_NAME, "generateSha");
    return new BigInteger(1, token).toString(16); // Hexadecimal encoding
  }

  /**
   * Method for creating a response for a bad format request.
   *
   * @param res The Response object.
   * @return A JSON String used for a bad format response.
   */
  private String setBadFormat(Response res) {
    logger.entering(CLASS_NAME, "setBadFormat");
    JSONObject json = new JSONObject();
    json.put("error", "bad format");
    logger.severe("error: Bad Request");
    res.status(400);
    logger.exiting(CLASS_NAME, "setBadFormat", json.toString());
    return json.toString();
  }
}
