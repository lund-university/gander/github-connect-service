package pur.service;

/** A Class for representing commits */
public class PurCommit {

  private String sha;
  private String content;

  /**
   * Constructor.
   *
   * @param sha The sha of the commit
   * @param content The content of the PurContent in the specific commit.
   */
  public PurCommit(String sha, String content) {
    this.sha = sha;
    this.content = content;
  }

  /**
   * Retrieves the sha of the commit.
   *
   * @return the sha of the commit.
   */
  public String getSha() {
    return this.sha;
  }

  /**
   * Gets the content of the commit.
   *
   * @return The String content of the commit.
   */
  public String getContent() {
    return this.content;
  }

  /**
   * Sets the content of the commit to newContent.
   *
   * @param newContent The new content String.
   */
  public void setContent(String content) {
    this.content = content;
  }

  @Override
  public int hashCode() {
    return sha.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj != null && obj instanceof PurCommit) {
      PurCommit tmpObj = (PurCommit) obj;
      if (tmpObj.getSha() != null && tmpObj.getSha().equals(this.sha)) {
        return true;
      }
    }
    return false;
  }
}
